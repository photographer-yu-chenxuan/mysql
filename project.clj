(defproject zjpjhx/mysql "0.1.0-SNAPSHOT"
  :description "mysql lib for clojure"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [mysql/mysql-connector-java "5.1.6"]
                 [korma "0.4.0"]])
